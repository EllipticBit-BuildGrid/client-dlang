module ellipticbit.buildgrid.apis;

import std.conv;
import std.string;
import std.utf;
import std.process;

version (Windows) {
private import core.sys.windows.com;
private import core.sys.windows.uuid;
private import core.sys.windows.objidl;
private import core.sys.windows.shlobj;

extern(C):
    //Types
    alias HANDLE = void*;
    alias HRESULT = int;
    alias DWORD = int;

    //Registry
    alias HKEY = HANDLE;

    private enum KEY_QUERY_VALUE        = 0x0001;
    private enum KEY_SET_VALUE          = 0x0002;
    private enum KEY_CREATE_SUB_KEY     = 0x0004;
    private enum KEY_ENUMERATE_SUB_KEYS = 0x0008;
    private enum KEY_NOTIFY             = 0x0010;
    private enum KEY_CREATE_LINK        = 0x0020;

    private enum KEY_READ = 0x20019;
    private enum KEY_WRITE = 0x20006;

    private enum HKEY_CURRENT_USER = 0x80000001;
    private enum HKEY_LOCAL_MACHINE = 0x80000002;

    private enum REG_CREATED_NEW_KEY            = 0x00000001L;   // New Registry Key created
    private enum REG_OPENED_EXISTING_KEY        = 0x00000002L;   // Existing Key opened

    private enum REG_NONE                       = 0U; // No value type
    private enum REG_SZ                         = 1U; // Unicode nul terminated string
    private enum REG_EXPAND_SZ                  = 2U; // Unicode nul terminated string (with environment variable references)
    private enum REG_BINARY                     = 3U; // Free form binary
    private enum REG_DWORD                      = 4U; // 32-bit number
    private enum REG_DWORD_LITTLE_ENDIAN        = 4U; // 32-bit number (same as REG_DWORD)
    private enum REG_DWORD_BIG_ENDIAN           = 5U; // 32-bit number
    private enum REG_LINK                       = 6U; // Symbolic Link (unicode)
    private enum REG_MULTI_SZ                   = 7U; // Multiple Unicode strings
    private enum REG_RESOURCE_LIST              = 8U; // Resource list in the resource map
    private enum REG_FULL_RESOURCE_DESCRIPTOR   = 9U; // Resource list in the hardware description
    private enum REG_RESOURCE_REQUIREMENTS_LIST = 10U;
    private enum REG_QWORD                      = 11U; // 64-bit number
    private enum REG_QWORD_LITTLE_ENDIAN        = 11U; // 64-bit number (same as REG_QWORD)

    private HRESULT RegGetValueA(HKEY hkey, const(char)* lpSubKey, const(char)* lpValue, DWORD dwFlags, DWORD* pdwType, void* pvData, DWORD* pcbData);

    extern(D) public string GetRegistryStringValue(string path, string name, bool useMachineKey) {
        DWORD strlen;
        RegGetValueA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)path.toStringz(), cast(char*)name.toStringz(), REG_SZ, null, null, &strlen);
        char[] str = new char[strlen];
        HRESULT hr = RegGetValueA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)path.toStringz(), cast(char*)name.toStringz(), REG_SZ, null, cast(void*)str.ptr, &strlen);
        return to!string(str[0..$-2]);
    }

    extern(D) public DWORD GetRegistryDWordValue(string path, string name, bool useMachineKey) {
		DWORD len = DWORD.sizeof;
		DWORD result = 0;
        HRESULT hr = RegGetValueA(cast(HKEY)(useMachineKey ? HKEY_LOCAL_MACHINE : HKEY_CURRENT_USER), cast(char*)path.toStringz(), cast(char*)name.toStringz(), REG_DWORD, null, cast(void*)&result, &len);
        return result;
    }
}
