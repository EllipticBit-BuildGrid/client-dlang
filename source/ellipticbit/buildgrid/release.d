module ellipticbit.buildgrid.release;

import ellipticbit.buildgrid.apis;
import ellipticbit.buildgrid.common;

import eventcore.driver;
import vibe.data.json;
import vibe.http.client;
import vibe.inet.urltransfer;

import std.conv;
import std.stdio;
import std.string;

public struct Release {
	@name("id") public long Id;
	@name("pid") public string PlatformId;
	@name("vid") public string VersionId;
	@name("i") public ubyte[] Icon;
	@name("iu") public string IconUrl;
	@name("m") public string Metadata;
	@name("rn") public string ReleaseNotes;
}

public struct ReleaseClient {
	public string ApiKey;
	public string PushKey;
	public string Tenant;
	public string Deployment;
	public string Channel;
}

public struct ReleaseSelectors {
	@name("os") public string Os;
	@name("osf") public string OsFlavor;
	@name("osma") public string OsMajor;
	@name("osmi") public string OsMinor;
	@name("osb") public string OsBuild;
	@name("arch") public string Architecture;
}

public RequestObjectResult!(Release[]) getReleases(ReleaseClient client) {
	string url = "https://buildgrid.io/api/releases/" ~ client.Tenant ~ "/" ~ client.Deployment ~ "/" ~ client.Channel;

	RequestObjectResult!(Release[]) retVal;
	requestHTTP(url,
		(scope request) {
			request.method = HTTPMethod.GET;
			request.headers["ApiKey"] = client.ApiKey;
			request.writeJsonBody!ReleaseSelectors(getReleaseSelectors());
		},
		(scope response) {
			retVal = RequestObjectResult!(Release[])(response, deserializeJson!(Release[])(response.readJson()));
		}
	);

	return retVal;
}

public RequestObjectResult!Release getRelease(ReleaseClient client, long releaseId) {
	string url = "https://buildgrid.io/api/releases/" ~ client.Tenant ~ "/" ~ client.Deployment ~ "/" ~ client.Channel ~ "/" ~ to!string(releaseId);

	RequestObjectResult!Release retVal;
	requestHTTP(url,
		(scope request) {
			request.method = HTTPMethod.GET;
			request.headers["ApiKey"] = client.ApiKey;
			request.writeJsonBody!ReleaseSelectors(getReleaseSelectors());
		},
		(scope response) {
			retVal = RequestObjectResult!Release(response, deserializeJson!Release(response.readJson()));
		}
	);

	return retVal;
}

public RequestObjectResult!Release getActiveRelease(ReleaseClient client) {
	string url = "https://buildgrid.io/api/releases/" ~ client.Tenant ~ "/" ~ client.Deployment ~ "/" ~ client.Channel ~ "/active";

	RequestObjectResult!Release retVal;
	requestHTTP(url,
		(scope request) {
			request.method = HTTPMethod.GET;
			request.headers["ApiKey"] = client.ApiKey;
			request.writeJsonBody!ReleaseSelectors(getReleaseSelectors());
		},
		(scope response) {
			retVal = RequestObjectResult!Release(response, deserializeJson!Release(response.readJson()));
		}
	);

	return retVal;
}

public RequestResult getPackage(ReleaseClient client, long releaseId, string outputPath) {
	string url = "https://buildgrid.io/api/releases/" ~ client.Tenant ~ "/" ~ client.Deployment ~ "/" ~ client.Channel ~ "/" ~ to!string(releaseId) ~ "/package";

	RequestResult retVal;
	requestHTTP(url,
		(scope request) {
			request.method = HTTPMethod.GET;
			request.headers["ApiKey"] = client.ApiKey;
		},
		(scope response) {
			retVal = RequestResult(response);

			auto f = File(outputPath, "wb");
			scope(exit) {
				f.flush();
				f.close();
			}

			ubyte[4096] buffer;
			while(!response.bodyReader.empty) {
				ulong rc = response.bodyReader.read(buffer, IOMode.all);
				f.rawWrite(buffer[0..rc]);
			}
		}
	);

	return retVal;
}

public RequestObjectResult!Release pushMetadata(ReleaseClient client, Release metadata) {
	string url = "https://buildgrid.io/api/releases/" ~ client.Tenant ~ "/" ~ client.Deployment ~ "/" ~ client.Channel ~ "/metadata";

	RequestObjectResult!Release retVal;
	requestHTTP(url,
		(scope request) {
			request.method = HTTPMethod.PUT;
			request.headers["PushKey"] = client.PushKey;
			request.contentType = "application/json";
			request.writeJsonBody!Release(metadata);
		},
		(scope response) {
			retVal = RequestObjectResult!Release(response, deserializeJson!Release(response.readJson()));
		}
	);

	return retVal;
}

public RequestResult pushPackage(ReleaseClient client, long releaseId, string filePath) {
	string url = "https://buildgrid.io/api/releases/" ~ client.Tenant ~ "/" ~ client.Deployment ~ "/" ~ client.Channel ~ "/" ~ to!string(releaseId) ~ "/package";

	RequestResult retVal;
	requestHTTP(url,
		(scope request) {
			request.method = HTTPMethod.PUT;
			request.headers["PushKey"] = client.PushKey;
			request.contentType = "application/octet-stream";

			auto f = File(filePath, "rb");
			scope(exit) {
				f.close();
			}

			ubyte[4096] buffer;
			while(!f.eof) {
				auto bf = f.rawRead(buffer);
				request.bodyWriter.write(bf);
			}

			request.bodyWriter.finalize();
		},
		(scope response) {
			retVal = RequestResult(response);
		}
	);

	return retVal;
}

public RequestResult setActiveRelease(ReleaseClient client, long releaseId) {
	string url = "https://buildgrid.io/api/releases/" ~ client.Tenant ~ "/" ~ client.Deployment ~ "/" ~ client.Channel ~ "/" ~ to!string(releaseId) ~ "/active";

	RequestResult retVal;
	requestHTTP(url,
		(scope request) {
			request.method = HTTPMethod.POST;
			request.headers["PushKey"] = client.PushKey;
		},
		(scope response) {
			retVal = RequestResult(response);
		}
	);

	return retVal;
}

private ReleaseSelectors getReleaseSelectors() {
	auto rs = ReleaseSelectors();

	version(Windows) {
		rs.Os = "windows";
		rs.OsFlavor = GetRegistryStringValue("Software\\Microsoft\\Windows NT\\CurrentVersion", "EditionID", true);
		rs.OsMajor = to!string(GetRegistryDWordValue("Software\\Microsoft\\Windows NT\\CurrentVersion", "CurrentMajorVersionNumber", true));
		rs.OsMinor = to!string(GetRegistryDWordValue("Software\\Microsoft\\Windows NT\\CurrentVersion", "CurrentMinorVersionNumber", true));
		rs.OsBuild = GetRegistryStringValue("Software\\Microsoft\\Windows NT\\CurrentVersion", "CurrentBuildNumber", true);
		rs.Architecture = GetRegistryStringValue("System\\CurrentControlSet\\Control\\Session Manager\\Environment", "PROCESSOR_ARCHITECTURE", true).toLower().replace("amd", "x86_");
	}
	version(linux) {
		auto archres = executeShell("uname -m");
		string[string] releaseLines;
		auto osReleaseLines = lineSplitter(readText("/etc/os-release")).array;
		foreach (string l; osReleaseLines) {
			auto s = l.split("=");
			releaseLines[s[0]] = s[1].strip().replace("\"", string.init);
		}
		auto versionSplits = releaseLines["VERSION_ID"].split(".");

		rs.Os = "linux";
		rs.OsFlavor = releaseLines["ID"];
		if (versionSplits.length > 0) rs.OsMajor = versionSplits[0];
		if (versionSplits.length > 1) rs.OsMinor = versionSplits[1];
		if (versionSplits.length > 2) rs.OsBuild = versionSplits[2];
		rs.Architecture = archres.output;
	}
	version(OSX) {
		auto archres = executeShell("uname -m");
		auto versionText = lineSplitter(executeShell("sw_vers").output).array;
		string[string] versionLines;
		foreach (string l; versionText) {
			auto s = l.split(":");
			versionLines[s[0]] = s[1].strip();
		}
		auto versionSplits = versionLines["ProductVersion"].split(".");

		rs.Os = "macos";
		if (versionSplits.length > 0) rs.OsMajor = versionSplits[0];
		if (versionSplits.length > 1) rs.OsMinor = versionSplits[1];
		if (versionSplits.length > 2) rs.OsBuild = versionSplits[2];
		rs.Architecture = archres.output;
	}

	return rs;
}
