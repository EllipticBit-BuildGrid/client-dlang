module ellipticbit.buildgrid.common;

import vibe.http.client;

public struct RequestResult {
	public int StatusCode;
	public string Reason;
	public string[string] Headers;

	public this(HTTPClientResponse response) {
		StatusCode = response.statusCode;
		Reason = response.statusPhrase;
		foreach (t; response.headers.byKeyValue) {
			Headers[t.key] = t.value;
		}
	}
}

public struct RequestObjectResult(T) {
	public int StatusCode;
	public string Reason;
	public string[string] Headers;
	public T Result;

	public this(HTTPClientResponse response, T object) {
		StatusCode = response.statusCode;
		Reason = response.statusPhrase;
		foreach (t; response.headers.byKeyValue) {
			Headers[t.key] = t.value;
		}
		Result = object;
	}
}
